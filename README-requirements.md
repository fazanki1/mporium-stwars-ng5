Angular coding test

The purpose of this is to assess the candidate’s coding skills. The look of the finished app is not as important as the code.

Test

Create an Angular app that displays the following Star Wars resources:
Films
People
Starships
Vehicles
Species
Planets
The app should use:
The Angular CLI 
ngrx to manage state
Angular Material and Flex Layout
The app should include:
Routing between the pages (each resource should be on a different page)
Display the first 10 results in each of the sections on the page
Push the resulting codebase to Github (or another code repository) and send the link to the code through to

Star Wars API

The Star Wars API can be found here: https://swapi.co.
